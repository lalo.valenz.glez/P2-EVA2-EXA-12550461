package com.mevg.eva2_examen_12550554;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase db;
    String pathSDCARD;
    TextView tvDatos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pathSDCARD = Environment.getExternalStorageDirectory().getAbsolutePath();
        //db = SQLiteDatabase.openDatabase(pathSDCARD+"/examen",null, SQLiteDatabase.CREATE_IF_NECESSARY);
        db = openOrCreateDatabase("examen",MODE_PRIVATE, null);
        tvDatos = (TextView) findViewById(R.id.datos);

        db.execSQL("drop table if exists alumnos");
        db.execSQL("create table if not exists alumnos(" +
                "nombre     text," +
                "apellidos  text," +
                "carrera    text," +
                "semestre   integer)");

        insertarAlumnos();
        for(int i = 0; i < readXml().size(); i++){
            Alumnos a = readXml().get(i);

            tvDatos.append("Nombre: " + a.getNombre()+", Apellidos: "+ a.getApellidos()+"\n");
            tvDatos.append("Semestre: " + a.getSemestre()+", Carrera: "+ a.getCarrera()+"\n");
        }
    }

    public void insertarAlumnos(){
        List<Alumnos> alumnosList = readXml();
        for (int i = 0; i < alumnosList.size(); i++ ){
            Alumnos alumno = alumnosList.get(i);
            db.execSQL("insert into alumnos values('"+alumno.getNombre()+"', '"+alumno.getApellidos()+"', '"+alumno.getCarrera()+"', '"+alumno.getSemestre()+"')");
        }
    }

    public List<Alumnos> readXml(){
        List<Alumnos> list = new ArrayList<>();
        Alumnos alumno = null;
        String text = "" ;
        try{
            XmlPullParser parser = getResources().getXml(R.xml.examen);
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("Alumno")) {

                            alumno = new Alumnos();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("Alumno")) {
                            // add employee object to list
                            list.add(alumno);
                        } else if (tagname.equalsIgnoreCase("Nombre")) {
                            alumno.setNombre(text);
                        } else if (tagname.equalsIgnoreCase("Apellidos")) {
                            alumno.setApellidos(text);
                        } else if (tagname.equalsIgnoreCase("Carrera")) {
                            alumno.setCarrera(text);
                        } else if (tagname.equalsIgnoreCase("Semestre")) {
                            alumno.setSemestre(Integer.parseInt(text));
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }
}
